<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 9/04/17
 * Time: 21:09
 */

namespace App\Seeder;

use App\Entity\Ad;
use App\Entity\Category;
use App\Entity\Content;
use App\Entity\Organization;
use App\Entity\Role;
use App\Entity\Store;
use App\Entity\Town;
use App\Entity\User;
use IBC\Kernel\EntityManager\DataManager\AdDataManager;
use IBC\Kernel\EntityManager\DataManager\CategoryDataManager;
use IBC\Kernel\EntityManager\DataManager\ContentDataManager;
use IBC\Kernel\EntityManager\DataManager\DataManager;
use IBC\Kernel\EntityManager\DataManager\FriendshipDataManager;
use IBC\Kernel\EntityManager\DataManager\DeveloperDataManager;
use IBC\Kernel\EntityManager\DataManager\OrganizationDataManager;
use IBC\Kernel\EntityManager\DataManager\StoreDataManager;
use IBC\Kernel\EntityManager\DataManager\TownDataManager;
use IBC\Kernel\EntityManager\DataManager\UserDataManager;

require_once __DIR__ .'/../../../app/kernel/init.php';


//Drop an existing schema and generate an empty one.
DeveloperDataManager::generateSchema();


//Generate and persist some towns on the schema.
$towns = array();

$benicolet = new Town(null, 'Benicolet', 'hola.png', '700');
$llutxent = new Town(null, 'Llutxent', null, '3500');
$beniganim = new Town(null, 'Benigànim', null, '15000');
$quatretonda = new Town(null, 'Quatretonda', null, '3200');
$castello_rugat = new Town(null, 'Castelló de Rugat', null, '7000');


$towns[] = $benicolet;
$towns[] = $llutxent;
$towns[] = $beniganim;
$towns[] = $quatretonda;
$towns[] = $castello_rugat;

foreach ($towns as $town){
    $town->persist();
}


//Generate and persist some friendships on the schema.

$benicolet->addFriend($llutxent);
$benicolet->addFriend($quatretonda);
$quatretonda->addFriend($castello_rugat);

var_dump(FriendshipDataManager::findFriendshipById(1));

var_dump($benicolet->getFriends());


//Generate and persist some users on the schema.
$users = array();

$isaac = new User(null, '20058148D', 'Isaac', 'Boscà', 'Canet',
                    '1994-10-04', 'M', 'isaacbncl@gmail.com', Role::getRole(Role::ROLE_SUPER_ADMIN), $benicolet, 'aa.jpg');
$isaac->setPassword('laliga2016');

$marta = new User(null, null, 'Marta', 'Alvaro', 'Arias',
    '1995-12-17', 'F', 'marta.alv.arias@gmail.com', Role::getRole(Role::ROLE_ADMIN), $benicolet);
$marta->setPassword('hola!');

$xels = new User(null, '19943215P', 'Carles', 'Gregori', 'Grau',
                '1993-10-25', 'M', 'xelsbnc@gmail.com', Role::getRole(Role::ROLE_USER), $llutxent);
$xels->setPassword('valhalla');

$lucia = new User(null, '52200014W', 'Lucía', 'Felipe', 'Gallego',
    '1993-05-31', 'F', 'lulille14@gmail.com', Role::getRole(Role::ROLE_USER), $llutxent);
$lucia->setPassword('lille');


$users[] = $isaac;
$users[] = $marta;
$users[] = $xels;
$users[] = $lucia;


foreach ($users as $user){
    $user->persist();
}


$benicoletUsers = UserDataManager::findUsersByTown($benicolet);
var_dump($benicoletUsers);

if($lucia->verifyPassword('lille')){
    echo 'Contraseña correcta';
}else{
    echo 'Esta contraseña no es válida';
}

//Generate and persist some organizations on the schema
$organizations = array();

$ames_casa = new Organization(null, 'Ames de Casa', 'Ames de Casa de Benicolet', $benicolet);
$musica = new Organization(null, 'Associació Musical', 'Associació musical de Benicolet', $benicolet);

$organizations[] = $ames_casa;
$organizations[] = $musica;

foreach ($organizations as $organization){
    $organization->persist();
}

$benicoletOrganizations = $benicolet->getOrganizations();
var_dump($benicoletOrganizations);

var_dump($isaac);
var_dump($isaac->getId());

$organization = OrganizationDataManager::findOrganizationByTownAndName($benicolet, 'Ames de Casa');
var_dump($organization);


//Generate and persist some organization members on the schema
$ames_casa->addMember($xels);
$musica->addMember($xels);
$musica->addMember($marta);
$ames_casa->addMember($lucia);

$ames_casaMembers = $ames_casa->getMembers();
var_dump($ames_casaMembers);

$xelsOrganizations = $xels->getOrganizations();
var_dump($xelsOrganizations);


//Execute custom find query
$customResult= DataManager::findCustomArray('SELECT * FROM user');
var_dump($customResult);


//Generate and persist some Stores
$stores = array();

$forn = new Store(null, 'Despatx de pa', $xels, 'El forn del poble', 'pa.png');
$bar_cooperativa = new Store(null, 'Bar cooperativa', $lucia, 'Un bar', 'birra.jpg');

$stores[] = $forn;
$stores[] = $bar_cooperativa;


foreach ($stores as $store){
    $store->persist();
}

var_dump(StoreDataManager::findStoreById(1));


//Generate and persist some Ads
$ads = array();

$pa = new Ad(null, $forn, 'Pa 50 centims', 'La barra de pa, a partir de hui valdrà 50 centims', 'pa_estalvi.jpg');
$cervessa = new Ad(null, $bar_cooperativa, 'Nova cervessa', 'Inclusio de cervessa Socarrada en Bar cooperativa', 'socarrada.png');
$tarta = new Ad(null, $forn, 'Tartes 20% de descompte', 'Tartes al 20% de descompte', 'pastís.jpg');

$ads[] = $pa;
$ads[] = $cervessa;
$ads[] = $tarta;

foreach ($ads as $ad){
    $ad->persist();
}

var_dump( AdDataManager::findAdById(1));

//Generate some visits to ads (viewed adfs)
$pa->viewedByUser($isaac);
$pa->viewedByUser($isaac);
$cervessa->viewedByUser($xels);
$tarta->viewedByUser($isaac);
$pa->viewedByUser($lucia);

$isaacViewedAds = $isaac->getViewedAds();

var_dump($isaacViewedAds);

$paWatchers = $pa->getWatchers();

var_dump($paWatchers);


//Generate and persist some Contents
$contents = array();

$content1 = new Content(null, 'Hello world!', 'Hello world desc.', Category::getCategory(1), null, $isaac, $ames_casa);
$content2 = new Content(null, 'De viatge', 'A formentera', Category::getCategory(2), null, $xels, $ames_casa);

$contents[] = $content1;
$contents[] = $content2;

foreach ($contents as $content){
    $content->persist();
}

var_dump(CategoryDataManager::findAll());

var_dump(ContentDataManager::findContentById(1));

$content1->readByUser($isaac);
$content1->readByUser($xels);
$content2->readByUser($marta);
$content1->readByUser($isaac);
$content2->readByUser($isaac);

var_dump($content1->getReaders());

var_dump($isaac->getReadContents());

$content1->addReceiver($benicolet);
$content2->addReceiver($llutxent);

var_dump(ContentDataManager::getContentsForUser($isaac));

var_dump(TownDataManager::getMultipleTownsByIds(array(1,2,3)));




