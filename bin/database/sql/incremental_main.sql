-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema townhub
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema townhub
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `townhub` DEFAULT CHARACTER SET utf8 ;
USE `townhub` ;

-- -----------------------------------------------------
-- Table `townhub`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `name`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`town`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`town` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `picture` VARCHAR(250) NULL DEFAULT NULL,
  `population` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` VARCHAR(15),
  `password` VARCHAR(250) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `firstSurname` VARCHAR(45) NOT NULL,
  `secondSurname` VARCHAR(45) NULL DEFAULT NULL,
  `birthdate` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(250) NOT NULL,
  `role` int(11) NOT NULL,
  `town` int(11) NOT NULL,
  `picture` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `dni_UNIQUE` (`dni` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_user_role_idx` (`role` ASC),
  INDEX `fk_user_town_idx` (`town` ASC),
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`role`)
    REFERENCES `townhub`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_town`
    FOREIGN KEY (`town`)
    REFERENCES `townhub`.`town` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`store`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(75) NOT NULL,
  `user` int(11) NOT NULL,
  `description` VARCHAR(250) NULL DEFAULT NULL,
  `logo` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `avoid_duplication` (`name` ASC, `user` ASC),
  INDEX `fk_store_user_idx` (`user` ASC),
  CONSTRAINT `fk_store_user`
    FOREIGN KEY (`user`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`ad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `title` VARCHAR(500) NOT NULL,
  `description` VARCHAR(500) NULL DEFAULT NULL,
  `photo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_ads_store_idx` (`store` ASC),
  CONSTRAINT `fk_ads_store`
    FOREIGN KEY (`store`)
    REFERENCES `townhub`.`store` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(300) NOT NULL,
  `author` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_question_author_idx` (`author` ASC),
  CONSTRAINT `fk_question_author`
    FOREIGN KEY (`author`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` int(11) NOT NULL,
  `description` VARCHAR(300) NOT NULL,
  `numberOfVotes` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_answers_question_idx` (`question` ASC),
  CONSTRAINT `fk_answers_question`
    FOREIGN KEY (`question`)
    REFERENCES `townhub`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(140) NOT NULL,
  `description` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`content`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(300) NOT NULL,
  `description` VARCHAR(900) NULL DEFAULT NULL,
  `category` int(11) NOT NULL,
  `createdAt` DATETIME NOT NULL,
  `author` int(11) NOT NULL,
  `organization` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_content_author_idx` (`author` ASC),
  INDEX `fk_content_category_idx` (`category` ASC),
  CONSTRAINT `fk_content_author`
    FOREIGN KEY (`author`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_content_category`
    FOREIGN KEY (`category`)
    REFERENCES `townhub`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_content_organization`
    FOREIGN KEY (`organization`)
    REFERENCES `townhub`.`organization` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`friendship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`friendship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town` int(11) NOT NULL,
  `friendTown` int(11) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `friendshipStartDate` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `avoid_duplication` (`town` ASC, `friendTown` ASC),
  INDEX `fk_friendship_friendTown_idx` (`friendTown` ASC),
  CONSTRAINT `fk_friendship_friendTown`
    FOREIGN KEY (`friendTown`)
    REFERENCES `townhub`.`town` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_friendship_town`
    FOREIGN KEY (`town`)
    REFERENCES `townhub`.`town` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`organization`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(300) NULL DEFAULT NULL,
  `logo` VARCHAR(250) NULL DEFAULT NULL,
  `town` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `uniqueNameTown` (`name` ASC, `town` ASC),
  CONSTRAINT `fk_organization_town` FOREIGN KEY (`id`) REFERENCES `town` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`organizationMember`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`organizationMember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `organization` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `avoid_duplications` (`user` ASC, `organization` ASC),
  INDEX `fk_organizationMember_organization_idx` (`organization` ASC),
  CONSTRAINT `fk_organizationMember_organization`
    FOREIGN KEY (`organization`)
    REFERENCES `townhub`.`organization` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_organizationMember_user`
    FOREIGN KEY (`user`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`read`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`read` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `avoid_duplication` (`user` ASC, `content` ASC),
  INDEX `fk_read_content_idx` (`content` ASC),
  CONSTRAINT `fk_read_content`
    FOREIGN KEY (`content`)
    REFERENCES `townhub`.`content` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_read_user`
    FOREIGN KEY (`user`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`receiver`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`receiver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` int(11) NOT NULL,
  `town` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `avoid_duplicarion` (`content` ASC, `town` ASC),
  INDEX `fk_receiver_town_idx` (`town` ASC),
  CONSTRAINT `fk_receiver_content`
    FOREIGN KEY (`content`)
    REFERENCES `townhub`.`content` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_receiver_town`
    FOREIGN KEY (`town`)
    REFERENCES `townhub`.`town` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`viewed`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`viewed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `ad` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_viewed_ad_idx` (`ad` ASC),
  INDEX `fk_viewed_user_idx` (`user` ASC),
  UNIQUE INDEX `avoid_duplicarion` (`user` ASC, `ad` ASC),
  CONSTRAINT `fk_viewed_ad`
    FOREIGN KEY (`ad`)
    REFERENCES `townhub`.`ad` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_viewed_user`
    FOREIGN KEY (`user`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `townhub`.`voted`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `townhub`.`voted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  PRIMARY KEY (`id`, `user`, `question`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `avoid_duplicated` (`user` ASC, `question` ASC),
  INDEX `fk_voted_question_idx` (`question` ASC),
  CONSTRAINT `fk_voted_question`
    FOREIGN KEY (`question`)
    REFERENCES `townhub`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_voted_user`
    FOREIGN KEY (`user`)
    REFERENCES `townhub`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Insert Roles
-- -----------------------------------------------------
INSERT INTO `role` (`id`, `name`) VALUES ('1', 'ROLE_SUPER_ADMIN');
INSERT INTO `role` (`id`, `name`) VALUES ('2', 'ROLE_ADMIN');
INSERT INTO `role` (`id`, `name`) VALUES ('3', 'ROLE_USER');

-- -----------------------------------------------------
-- Insert Categories
-- -----------------------------------------------------
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('1', 'Culture', 'Culture');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('2', 'Activities', 'Activities');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('3', 'Education', 'Education');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('4', 'Sports', 'Sports');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('5', 'Lost objects', 'Lost objects');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('6', 'Warning', 'Warning');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('7', 'Info', 'Info');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('8', 'Administration', 'Administration');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('9', 'Health', 'Health');
INSERT INTO `category` (`id`, `name`, `description`) VALUES ('10', 'Others', 'Others');


