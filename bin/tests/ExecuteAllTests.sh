#!/bin/bash

this="ExecuteAllTests.sh"
BASEDIR=$(dirname $0)

for script in $(dirname "$0")/*.sh;
do
    current=`basename "$script"`

    if [ $current != $this ]
    then
        ${BASEDIR}/${current}
    fi
done
