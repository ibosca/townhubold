<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 27/05/17
 * Time: 17:48
 */

namespace tests\entityManagerTest;


use IBC\Kernel\EntityManager\DataManager\TownDataManager;
use PHPUnit\Framework\TestCase;
use App\Entity\Town;
use IBC\Kernel\EntityManager\DataManager\DeveloperDataManager;
use IBC\Kernel\EntityManager\DataManager\FriendshipDataManager;
use App\Entity\User;
use App\Entity\Role;
use IBC\Kernel\EntityManager\DataManager\UserDataManager;
use App\Entity\Organization;
use IBC\Kernel\EntityManager\DataManager\OrganizationDataManager;
use IBC\Kernel\EntityManager\DataManager\DataManager;
use App\Entity\Store;
use IBC\Kernel\EntityManager\DataManager\StoreDataManager;
use App\Entity\Ad;
use IBC\Kernel\EntityManager\DataManager\AdDataManager;
use App\Entity\Content;
use IBC\Kernel\EntityManager\DataManager\ContentDataManager;
use App\Entity\Category;
use IBC\Kernel\EntityManager\DataManager\CategoryDataManager;

require_once __DIR__ . '/../../app/kernel/parameters/initParameters.php';
include_once BASEPATH .'app/kernel/autoloader/initAutoload.php';

class ORMPersistFindCompleteTest extends TestCase
{

    public function testORM()
    {
        //Drop an existing schema and generate an empty one.
        DeveloperDataManager::generateSchema();

        $towns = $this->generateAndPersistTowns();
        $this->generateFriendships($towns);
        $users = $this->generateUsers($towns);
        $this->verifyUserPassword($users[3]);
        $organizations = $this->generateOrganizations($towns);
        $this->generateMembers($organizations, $users);
        $this->customQuery();
        $stores = $this->generateStores($users);
        $ads = $this->generateAds($stores);
        $this->generateVisitsToAds($ads, $users);
        $contents = $this->generateContents($users, $organizations);
        $this->findCategories();
        $this->generateVisitsToContents($contents, $users);
        $this->generateReceivers($contents, $towns, $users);
        $this->findMultipleTowns();

    }

    public function generateAndPersistTowns()
    {
        //Generate and persist some towns on the schema.
        $towns = array();

        $benicolet = new Town(null, 'Benicolet', 'hola.png', '700');
        $llutxent = new Town(null, 'Llutxent', null, '3500');
        $beniganim = new Town(null, 'Benigànim', null, '15000');
        $quatretonda = new Town(null, 'Quatretonda', null, '3200');
        $castello_rugat = new Town(null, 'Castelló de Rugat', null, '7000');


        $towns[] = $benicolet;
        $towns[] = $llutxent;
        $towns[] = $beniganim;
        $towns[] = $quatretonda;
        $towns[] = $castello_rugat;

        foreach ($towns as $town){
            $town->persist();
        }

        $llutxentDB = TownDataManager::findTownByName('Llutxent')[0];

        $this->assertEquals($llutxent, $llutxentDB);

        return $towns;
    }

    public function generateFriendships($towns)
    {
        $towns[0]->addFriend($towns[1]);

        $findFriend = FriendshipDataManager::findFriendshipById(1)[0]->friendTown;
        $benicoletFriends = $towns[0]->getFriends()[0];

        $this->assertEquals($findFriend, $benicoletFriends);

    }

    public function generateUsers($towns)
    {
        $users = array();

        $isaac = new User(null, '20058148D', 'Isaac', 'Boscà', 'Canet',
            '1994-10-04', 'M', 'isaacbncl@gmail.com', Role::getRole(Role::ROLE_SUPER_ADMIN), $towns[0], 'aa.jpg');
        $isaac->setPassword('laliga2016');

        $marta = new User(null, null, 'Marta', 'Alvaro', 'Arias',
            '1995-12-17', 'F', 'marta.alv.arias@gmail.com', Role::getRole(Role::ROLE_ADMIN), $towns[0]);
        $marta->setPassword('hola!');

        $xels = new User(null, '19943215P', 'Carles', 'Gregori', 'Grau',
            '1993-10-25', 'M', 'xelsbnc@gmail.com', Role::getRole(Role::ROLE_USER), $towns[0]);
        $xels->setPassword('valhalla');

        $lucia = new User(null, '52200014W', 'Lucía', 'Felipe', 'Gallego',
            '1993-05-31', 'F', 'lulille14@gmail.com', Role::getRole(Role::ROLE_USER), $towns[1]);
        $lucia->setPassword('lille');

        $users[] = $isaac;
        $users[] = $marta;
        $users[] = $xels;
        $users[] = $lucia;


        foreach ($users as $user){
            $user->persist();
        }

        $isaacBnc = UserDataManager::findUsersByTown($towns[0])[0];


        $this->assertEquals($isaacBnc->email, $users[0]->email);

        return $users;

    }

    public function verifyUserPassword($user)
    {
        $this->assertTrue($user->verifyPassword('lille'));
    }

    public function generateOrganizations($towns)
    {
        $organizations = array();

        $ames_casa = new Organization(null, 'Ames de Casa', 'Ames de Casa de Benicolet', $towns[0]);
        $musica = new Organization(null, 'Associació Musical', 'Associació musical de Benicolet', $towns[0]);

        $organizations[] = $ames_casa;
        $organizations[] = $musica;

        foreach ($organizations as $organization){
            $organization->persist();
        }

        $amesCasaBnc = $towns[0]->getOrganizations()[0];


        $organization = OrganizationDataManager::findOrganizationByTownAndName($towns[0], 'Ames de Casa')[0];

        $this->assertEquals($amesCasaBnc, $organization);

        return $organizations;
    }

    public function generateMembers($organizations, $users)
    {

        $organizations[0]->addMember($users[1]);
        $organizations[0]->addMember($users[2]);

        $user = $organizations[0]->getMembers()[0];

        $this->assertEquals($user->email, $users[1]->email);

        $org = $users[2]->getOrganizations()[0];

        $this->assertEquals($org, $organizations[0]);

        $emptyOrgs = $users[3]->getOrganizations();
        $this->assertEmpty($emptyOrgs);

    }

    public function customQuery()
    {
        $customResult= DataManager::findCustomArray('SELECT * FROM user');

        $this->assertNotEmpty($customResult);

        $this->assertArrayHasKey('email', $customResult[0]);

    }

    public function generateStores($users)
    {
        $stores = array();

        $forn = new Store(null, 'Despatx de pa', $users[1], 'El forn del poble', 'pa.png');
        $bar_cooperativa = new Store(null, 'Bar cooperativa', $users[2], 'Un bar', 'birra.jpg');

        $stores[] = $forn;
        $stores[] = $bar_cooperativa;


        foreach ($stores as $store){
            $store->persist();
        }

        $fornDB = StoreDataManager::findStoreById(1)[0];

        $this->assertEquals($forn->name, $fornDB->name);

        return $stores;
    }

    public function generateAds($stores)
    {
        $ads = array();

        $pa = new Ad(null, $stores[0], 'Pa 50 centims', 'La barra de pa, a partir de hui valdrà 50 centims', 'pa_estalvi.jpg');
        $cervessa = new Ad(null, $stores[1], 'Nova cervessa', 'Inclusio de cervessa Socarrada en Bar cooperativa', 'socarrada.png');
        $tarta = new Ad(null, $stores[0], 'Tartes 20% de descompte', 'Tartes al 20% de descompte', 'pastís.jpg');

        $ads[] = $pa;
        $ads[] = $cervessa;
        $ads[] = $tarta;

        foreach ($ads as $ad){
            $ad->persist();
        }

        $adFromDb = AdDataManager::findAdById(1)[0];

        $this->assertEquals($ads[0]->title, $adFromDb->title);

        return $ads;

    }

    public function generateVisitsToAds($ads, $users)
    {
        $visits = array();

        $ads[0]->viewedByUser($users[0]);
        $ads[0]->viewedByUser($users[0]);
        $ads[1]->viewedByUser($users[1]);
        $ads[2]->viewedByUser($users[0]);

        $isaacViewedAds = $users[0]->getViewedAds();


        $this->assertEquals(count($isaacViewedAds), 2);

        $paWatchers = $ads[0]->getWatchers();

        $this->assertEquals(count($paWatchers), 1);

    }

    public function generateContents($users, $organizations)
    {
        $contents = array();

        $content1 = new Content(null, 'Hello world!', 'Hello world desc.', Category::getCategory(1), null, $users[1], $organizations[1]);
        $content2 = new Content(null, 'De viatge', 'A formentera', Category::getCategory(2), null, $users[1], $organizations[1]);

        $contents[] = $content1;
        $contents[] = $content2;

        foreach ($contents as $content){
            $content->persist();
        }

        $contentDB = ContentDataManager::findContentById(1)[0];

        $this->assertEquals('marta.alv.arias@gmail.com', $contentDB->getAuthor()->email);

        return $contents;

    }

    public function findCategories()
    {
        $categories = CategoryDataManager::findAll();

        $this->assertEquals("App\\Entity\\Category", get_class($categories[1]));
    }

    public function generateVisitsToContents($contents, $users)
    {
        $contents[0]->readByUser($users[2]);
        $contents[1]->readByUser($users[2]);

        $readers = $contents[0]->getReaders();
        $this->assertEquals(count($readers), 1);

        $readedContents = $users[2]->getReadContents();
        $this->assertEquals(count($readedContents), 2);

    }

    public function generateReceivers($contents, $towns, $users)
    {
        $contents[0]->addReceiver($towns[1]);

        $contents = ContentDataManager::getContentsForUser($users[3]);

        $this->assertEquals(count($contents), 1);
        $this->assertEquals($contents[0]->getAuthor()->email, 'marta.alv.arias@gmail.com');


    }

    public function findMultipleTowns()
    {
        $towns = TownDataManager::getMultipleTownsByIds(array(1,2,3));
        $this->assertCount(3, $towns);
        $this->assertEquals(get_class($towns[0]), "App\\Entity\\Town");

    }


}