<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 19/05/17
 * Time: 23:08
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Ad;
use App\Entity\User;

class AdDataManager extends DataManager
{
    const AD_SELECT = 'SELECT
		                ad.id				AS Ad$id_ad,
                        ad.title			AS Ad$title,
                        ad.description		AS Ad$description,
                        ad.photo			AS Ad$photo,
                        store.id            AS Store$id_store,
                        store.name          AS Store$name,
                        store.description   AS Store$description,
                        store.logo          AS Store$logo,
                        store.user			AS Store$user ';


    public static function persistAd(Ad $ad)
    {

        $query = "INSERT INTO ad (store, title, description, photo) VALUES (?, ?, ?, ?)";

        $adId = DataManager::persist($query, $ad);

        return $adId;

    }


    protected static function bindAdParams($params)
    {

        $stmt = $params[0];
        $ad = $params[1];

        $store = $ad->store->getId();

        $stmt->bind_param('isss', $store, $ad->title, $ad->description, $ad->photo);

        return $stmt;

    }

    public static function findAdById($id)
    {

        $query =  self::AD_SELECT . 'FROM townhub.ad
                  LEFT JOIN store ON ad.store = store.id
                  WHERE ad.id = ?';

        $ad = DataManager::find($query, $params = array('i', $id), 'Ad');

        return $ad;

    }

    public static function findViewedAdsByUser(User $user)
    {
        $query = self::AD_SELECT . 'FROM townhub.ad
                  LEFT JOIN store ON ad.store = store.id
                  LEFT JOIN viewed ON ad.id = viewed.ad
                  WHERE viewed.user = ?';
        
        $userId = $user->getId();
        
        $ads = DataManager::find($query, $params = array('i', $userId), 'Ad');
        
        return $ads;

    }


}