<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 25/05/17
 * Time: 9:19
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Read;

class ReadDataManager extends DataManager
{

    public static function persistRead(Read $read)
    {
        $query = 'INSERT IGNORE INTO `read` (user, content) VALUES (?, ?)';

        $readId = DataManager::persist($query, $read);

        return $readId;
    }

    protected static function bindReadParams($params)
    {
        $stmt = $params[0];
        $read = $params[1];

        $userId = $read->user->getId();
        $contentId = $read->content->getId();

        $stmt->bind_param('ii', $userId, $contentId);

        return $stmt;
    }


}