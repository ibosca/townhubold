<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 7/05/17
 * Time: 17:33
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Organization;
use App\Entity\OrganizationMember;
use App\Entity\User;

class OrganizationMemberDataManager extends DataManager
{

    public static function persistOrganizationMember(OrganizationMember $member)
    {
        $query = "INSERT INTO organizationMember (user, organization) VALUES (?, ?)";

        $memberId = DataManager::persist($query, $member);

        return $memberId;
    }

    protected static function bindOrganizationMemberParams($params)
    {

        $stmt = $params[0];
        $member = $params[1];

        $user = $member->user->getId();
        $organiztion = $member->organization->getId();

        $stmt->bind_param('ii', $user, $organiztion);

        return $stmt;

    }

    public static function findMembersByOrganization(Organization $organization)
    {

        $query = UserDataManager::USER_SELECT . 'FROM user
                  LEFT JOIN role               ON user.role = role.id
                  LEFT JOIN town               ON user.town = town.id
                  LEFT JOIN organizationMember ON user.id = organizationMember.user
                  WHERE organizationMember.organization = ?';

        $organizationId = $organization->getId();

        $members = DataManager::find($query, $params = array('i', $organizationId), 'User');

        return $members;

    }

    public static function findOrganizationsByUser(User $user)
    {

        $query = OrganizationDataManager::ORGANIZATION_SELECT . 'FROM organization
                  LEFT JOIN town ON organization.town = town.id
                  LEFT JOIN organizationMember ON organization.id = organizationMember.organization
                  WHERE organizationMember.user = ?';

        $userId = $user->getId();

        $organizations = DataManager::find($query, $params = array('i', $userId), 'Organization');

        return $organizations;

    }

}