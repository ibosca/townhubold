<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 5/05/17
 * Time: 23:44
 */

namespace IBC\Kernel\EntityManager\DataManager;

use App\Entity\Ad;
use App\Entity\Content;
use App\Entity\Town;
use App\Entity\User;


class UserDataManager extends DataManager
{

    const USER_SELECT = 'SELECT 
                          user.id				AS User$id_user,
	                      user.dni				AS User$dni,
                          user.name			    AS User$name,
                          user.firstSurname	    AS User$firstSurname,
                          user.secondSurname	AS User$secondSurname,
                          user.birthdate		AS User$birthdate,
                          user.gender			AS User$gender,
                          user.email			AS User$email,
                          role.id               AS Role$id,
                          role.name			    AS Role$name,
                          town.id            	AS Town$id_town,
	                      town.name             AS Town$name,
	                      town.picture 	        AS Town$picture,
	                      town.population       AS Town$population,
                          user.picture			AS User$picture ';

    public static function persistUser(User $user)
    {

        $query = "INSERT INTO user (dni, password, name, firstSurname, secondSurname, birthdate, gender, email, role, town, picture) 
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $userId = DataManager::persist($query, $user);

        return $userId;

    }

    protected static function bindUserParams($params)
    {

        $stmt = $params[0];
        $user = $params[1];

        $town = $user->town->getId();
        $role = $user->role->getId();

        $stmt->bind_param(
            'ssssssssiis',
            $user->dni,
            $user->password,
            $user->name,
            $user->firstSurname,
            $user->secondSurname,
            $user->birthdate,
            $user->gender,
            $user->email,
            $role,
            $town,
            $user->picture);

        return $stmt;

    }

    public static function findUsersByTown(Town $town)
    {
        $query = self::USER_SELECT . 'FROM user
                  LEFT JOIN role ON user.role = role.id
                  LEFT JOIN town ON user.town = town.id
                  WHERE town.id = ?';

        $townId = $town->getId();

        $users = DataManager::find($query, $params = array('i', $townId), 'User');

        return $users;

    }

    public static function findUserByEmail($email)
    {

        $query = self::USER_SELECT . 'FROM user
                  LEFT JOIN role ON user.role = role.id
                  LEFT JOIN town ON user.town = town.id
                  WHERE user.email = ?';

        $user = DataManager::find($query, $params = array('s', $email), 'User');

        return $user;
    }

    public static function findWatchersOfAd(Ad $ad)
    {
        $query = self::USER_SELECT . 'FROM user
                  LEFT JOIN role ON user.role = role.id
                  LEFT JOIN town ON user.town = town.id
                  LEFT JOIN viewed ON user.id = viewed.user
                  WHERE viewed.ad = ?';

        $adId = $ad->getId();

        $users = DataManager::find($query, $params = array('i', $adId), 'User');

        return $users;


    }

    public static function findReadersOfContent(Content $content)
    {

        $query = self::USER_SELECT . 'FROM user
                  LEFT JOIN role ON user.role = role.id
                  LEFT JOIN town ON user.town = town.id
                  LEFT JOIN `read` ON user.id = `read`.user
                  WHERE `read`.content = ?';

        $contentId = $content->getId();

        $users = DataManager::find($query, $params = array('i', $contentId), 'User');

        return $users;

    }

}