<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 13/04/17
 * Time: 10:59
 */

namespace IBC\Kernel\EntityManager\DataManager;


class DeveloperDataManager extends DataManager
{

    public static function generateSchema()
    {

        if(ENV === 'DEV'){

            DeveloperDataManager::dropDatabase(DB_NAME);
            DeveloperDataManager::executeSQLFile(BASEPATH."bin/database/sql/incremental_main.sql");


        }else{
            echo "This method can't be executed on PROD environment. Please, switch to DEV and try again.";
        }


    }

    protected function executeSQLFile($script_path)
    {

        $command = 'mysql'
            . ' --host=' . DB_HOST
            . ' --user=' . DB_USER
            . ' --password=' . DB_PASS
            . ' --execute="SOURCE ' . $script_path.'"'
        ;

        shell_exec($command);


    }

    protected function dropDatabase($dbName)
    {

        DataManager::executeQuery('DROP DATABASE IF EXISTS '.$dbName);

    }


}