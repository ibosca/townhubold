<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 20/05/17
 * Time: 11:48
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Viewed;

class ViewedDataManager extends DataManager
{

    public static function persistViewed(Viewed $viewed)
    {
        $query = 'INSERT IGNORE INTO  viewed (user, ad) VALUES(?, ?)';

        $viewedId = DataManager::persist($query, $viewed);

        return $viewedId;
    }

    protected static function bindViewedParams($params)
    {
        $stmt = $params[0];
        $viewed = $params[1];

        $userId = $viewed->user->getId();
        $adId = $viewed->ad->getId();

        $stmt->bind_param('ii', $userId, $adId);

        return $stmt;
    }



}