<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 25/05/17
 * Time: 11:34
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Receiver;

class ReceiverDataManager extends DataManager
{

    public static function persistReceiver(Receiver $receiver)
    {
        $query = "INSERT INTO receiver (`content`, `town`) VALUES (?, ?);";

        $receiverId = DataManager::persist($query, $receiver);

        return $receiverId;

    }

    protected function bindReceiverParams($params)
    {

        $stmt = $params[0];
        $receiver = $params[1];

        $content = $receiver->content->getId();
        $town = $receiver->town->getId();

        $stmt->bind_param('ii', $content, $town);

        return $stmt;

    }

}