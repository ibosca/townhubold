<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 11/05/17
 * Time: 22:38
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Store;

class StoreDataManager extends DataManager
{
    const STORE_SELECT = 'SELECT
		                  store.id              AS Store$id_store,
                          store.name            AS Store$name,
                          store.description     AS Store$description,
                          store.logo            AS Store$logo,
                          user.id				AS User$id_user,
	                      user.dni				AS User$dni,
                          user.name			    AS User$name,
                          user.firstSurname	    AS User$firstSurname,
                          user.secondSurname	AS User$secondSurname,
                          user.birthdate		AS User$birthdate,
                          user.gender			AS User$gender,
                          user.email			AS User$email,
                          role.name			    AS User$role,
                          user.town				AS User$town,
                          user.picture			AS User$picture ';

    public static function persistStore(Store $store)
    {
        $query = 'INSERT INTO store(name, user, description, logo) VALUES (?, ?, ?, ?)';

        $storeId = DataManager::persist($query, $store);

        return $storeId;

    }

    protected static function bindStoreParams($params)
    {
        $stmt = $params[0];
        $store = $params[1];

        $userId = $store->user->getId();

        $stmt->bind_param('siss', $store->name, $userId, $store->description, $store->logo);

        return $stmt;

    }

    public static function findStoreById($id)
    {

        $query = self::STORE_SELECT . 'FROM store
                  LEFT JOIN user ON store.user = user.id
                  LEFT JOIN role ON user.role = role.id
                  WHERE store.id = ?';

        $store = DataManager::find($query, $params = array('i', $id), 'Store');

        return $store;

    }

    public static function findStoreByNameAndUser($name, $user)
    {

        $query = self::STORE_SELECT . 'FROM store
                  LEFT JOIN user ON store.user = user.id
                  LEFT JOIN role ON user.role = role.id
                  WHERE store.name = ?
                  AND user.id = ?';

        $userId = $user->getId();

        $store = DataManager::find($query, $params = array('si', $name, $userId), 'Store');

        return $store;

    }

}