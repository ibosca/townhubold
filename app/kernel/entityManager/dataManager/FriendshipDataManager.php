<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 26/04/17
 * Time: 17:47
 */

namespace IBC\Kernel\EntityManager\DataManager;

use App\Entity\Friendship;


class FriendshipDataManager extends DataManager
{

    const FRIENDSHIP_SELCT = 'SELECT 
                          friendship.id      			  AS Friendship$id,	
                          town.id            			  AS Town$id,
                          town.name          			  AS Town$name,
                          town.picture       			  AS Town$picture,
                          town.population                 AS Town$population,
                          friendTown.id      			  AS FriendTown$id,
                          friendTown.name    			  AS FriendTown$name,
                          friendTown.picture 			  AS FriendTown$picture,
                          friendTown.population           AS FriendTown$population,
                          friendship.status  			  AS Friendship$status,
                          friendship.friendshipStartDate  AS Friendship$date ';


    protected static function bindFriendshipParams($params)
    {

        $stmt = $params[0];
        $friendship = $params[1];

        $town = $friendship->town->getId();
        $friendTown = $friendship->friendTown->getId();
        $status = $friendship->status;

        $stmt->bind_param('iii', $town, $friendTown, $status);

        return $stmt;

    }

    public static function persistFriendship(Friendship $friendship)
    {

        $query = "INSERT INTO friendship (town, friendTown, status, friendshipStartDate) VALUES (?, ?, ?, CURDATE())";

        $friendshipId = DataManager::persist($query, $friendship);

        return $friendshipId;
    }

    public static function findFriendshipById($id)
    {

        $query = self::FRIENDSHIP_SELCT . 'FROM friendship
                  LEFT JOIN town ON town = town.id
                  LEFT JOIN town friendTown ON friendTown = friendTown.id
                  WHERE friendship.id = ?';

        $friendship = DataManager::find($query, $params = array('i', $id), 'Friendship');

        return $friendship;

    }





}