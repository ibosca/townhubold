<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 21/05/17
 * Time: 14:09
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Content;
use App\Entity\User;

class ContentDataManager extends DataManager
{
    const CONTENT_SELECT = 'SELECT   
                           content.id			    AS Content$id_content,
                           content.title		    AS Content$title,
                           content.description	    AS Content$description,
                           category.id			    AS Category$id_category,
                           category.name		    AS Category$name,
                           category.description	    AS Category$description,
                           content.createdAt	    AS Content$createdAt,
                           author.id			    AS Author$id_user,
	                       author.dni			    AS Author$dni,
                           author.name			    AS Author$name,
                           author.firstSurname	    AS Author$firstSurname,
                           author.secondSurname	    AS Author$secondSurname,
                           author.birthdate		    AS Author$birthdate,
                           author.gender		    AS Author$gender,
                           author.email			    AS Author$email,
                           author.role              AS Author$role,
                           author.town              AS Author$town,
                           author.picture	        AS Author$picture,
                           organization.id          AS Organization$id,
						   organization.name        AS Organization$name,
                           organization.description AS Organization$description,
                           organization.town	    AS Organization$town,
                           organization.logo        AS Organization$logo ';

    public static function persistContent(Content $content)
    {
        $query = "INSERT INTO content (title, description, category, createdAt, author, organization) VALUES (?, ?, ?, NOW(), ?, ?)";

        $contentId = DataManager::persist($query, $content);

        return $contentId;

    }

    protected function bindContentParams($params)
    {

        $stmt = $params[0];
        $content = $params[1];

        $author = $content->author->getId();
        $category = $content->category->getId();
        $organization = $content->organization->getId();

        $stmt->bind_param('ssiii', $content->title, $content->description, $category, $author, $organization);

        return $stmt;

    }

    public static function findContentById($id)
    {
        $query = self::CONTENT_SELECT . 'FROM content
                 LEFT JOIN category ON content.category = category.id
                 LEFT JOIN organization ON content.organization = organization.id
                 LEFT JOIN user AS author ON content.author = author.id
                 WHERE content.id = ?';

        $content = DataManager::find($query, $params = array('i', $id), 'Content');

        return $content;

    }

    public static function findReadContentsByUser(User $user)
    {
        $query = self::CONTENT_SELECT . 'FROM content
                 LEFT JOIN category ON content.category = category.id
                 LEFT JOIN user AS author ON content.author = author.id
                 LEFT JOIN organization ON content.organization = organization.id
                 LEFT JOIN `read` ON content.id = `read`.content 
                 WHERE `read`.user = ?';

        $userId = $user->getId();

        $contents = DataManager::find($query, $params = array('i', $userId), 'Content');

        return $contents;

    }

    public static function getContentsForUser(User $user)
    {

        $query = self::CONTENT_SELECT . 'FROM content
                 LEFT JOIN category ON content.category = category.id
                 LEFT JOIN user AS author ON content.author = author.id
                 LEFT JOIN organization ON content.organization = organization.id
                 LEFT JOIN receiver ON content.id = receiver.content
                 WHERE receiver.town = ?';

        $town = $user->town->getId();

        $contents = DataManager::find($query, $params = array('i', $town), 'Content');

        return $contents;

    }



}