<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 9/04/17
 * Time: 10:30
 */

namespace IBC\Kernel\EntityManager\DataManager;

use IBC\Kernel\EntityManager\PropertyObject;
use mysqli;
use ReflectionClass;


class DataManager extends PropertyObject
{

    private static $dbHost = DB_HOST;
    private static $dbUser = DB_USER;
    private static $dbPass = DB_PASS;
    private static $dbName = DB_NAME;

    //METHODS:
    protected static function openConnection()
    {

        $conn = new mysqli(self::$dbHost, self::$dbUser, self::$dbPass, self::$dbName);
        $conn->set_charset('utf8');

        if ($conn->connect_error) {
            echo "Database connection error";
            exit();
        }

        return $conn;

    }

    protected static function closeConection($conn)
    {
        $conn->close();
    }

    protected static function executeQuery($query)
    {
        $conn = self::openConnection();

        $result = $conn->query($query);

        self::closeConection($conn);
        return $result;
    }

    protected function persist($query, $object)
    {

        $conn = self::openConnection();

        if ($stmt = $conn->prepare($query)) {

            $stmt = self::bindParams($stmt, $object);

            $stmt->execute();
            $objectId = $conn->insert_id;
            $stmt->close();

        }

        self::closeConection($conn);

        return $objectId;


    }

    protected static function find($query, $params, $objectType)
    {

        $conn = self::openConnection();

        if ($stmt = $conn->prepare($query)) {

            if($params != null){
                call_user_func_array(array($stmt, 'bind_param'), self::refValues($params));
            }


            $stmt->execute();


            if($stmt->error != null){
                echo '<h1>ERROR EN CONSULTA</h1>';
                die();
            }

            $result = $stmt->get_result();

            //$objects = call_user_func('IBC\Kernel\EntityManager\PropertyObject::build' . $objectType , $result);
            $objects = self::buildObject($result, $objectType);

            $stmt->close();
            self::closeConection($conn);

            return $objects;

        }else{
            echo '<h1>ERROR EN PREPARE</h1>';
            die();
        }

    }

    protected static function refValues($params)
    {

        $refs = array();
        foreach ($params as $key => $value)
            $refs[$key] = &$params[$key];

        return $refs;
    }

    protected static function bindParams($stmt, $object)
    {

        $reflect = new ReflectionClass($object);
        $objectType = $reflect->getShortName();

        $stmt = call_user_func(__NAMESPACE__ . '\\' . $objectType . 'DataManager::bind' . $objectType . 'Params', array($stmt, $object));

        return $stmt;

    }

    public static function findCustomArray($query, $params = null)
    {

        $array = self::find($query, $params, 'Array');

        return $array;


    }


}