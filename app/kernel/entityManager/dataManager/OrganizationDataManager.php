<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 7/05/17
 * Time: 0:30
 */

namespace IBC\Kernel\EntityManager\DataManager;


use App\Entity\Organization;
use App\Entity\Town;

class OrganizationDataManager extends DataManager
{
    const ORGANIZATION_SELECT = 'SELECT
                          organization.id          AS Organization$id,
	                      organization.name        AS Organization$name,
                          organization.description AS Organization$description,
                          town.id                  AS Town$id,
                          town.name                AS Town$name,
                          town.picture             AS Town$picture,
                          town.population          AS Town$population,
                          organization.logo        AS Organization$logo ';

    public static function persistOrganization(Organization $organization)
    {

        $query = "INSERT INTO organization (name, description, logo, town) VALUES (?, ?, ?, ?)";

        $organizationId = DataManager::persist($query, $organization);

        return $organizationId;
    }

    protected static function bindOrganizationParams($params)
    {

        $stmt = $params[0];
        $organization = $params[1];

        $town = $organization->town->getId();

        $stmt->bind_param('sssi', $organization->name, $organization->description, $organization->logo, $town);

        return $stmt;

    }

    public static function findOrganizationsByTown(Town $town)
    {

        $query = self::ORGANIZATION_SELECT . 'FROM organization
                  LEFT JOIN town ON organization.town = town.id
                  WHERE town.id = ?';

        $townId = $town->getId();

        $organizations = DataManager::find($query, $params = array('i', $townId), 'Organization');

        return $organizations;


    }

    public static function findOrganizationByTownAndName(Town $town, $name)
    {
        $query = self::ORGANIZATION_SELECT . 'FROM organization
                  LEFT JOIN town ON organization.town = town.id
                  WHERE town.id = ?
                  AND organization.name = ?';

        $townId = $town->getId();

        $organization = DataManager::find($query, $params = array('is', $townId, $name), 'Organization');

        return $organization;

    }


}