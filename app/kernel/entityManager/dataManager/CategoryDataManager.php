<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 24/05/17
 * Time: 11:48
 */

namespace IBC\Kernel\EntityManager\DataManager;


class CategoryDataManager extends DataManager
{
    const CATEGORY_SELECT = 'SELECT
                          category.id			AS Category$id,
                          category.name			AS Category$name,
                          category.description	AS Category$description ';

    public static function findCategoryById($id)
    {
        $query = self::CATEGORY_SELECT . 'FROM category
                  WHERE category.id = ?';

        $category = DataManager::find($query, $params = array('i', $id), 'Category');

        return $category;

    }

    public static function findAll()
    {
        $query = self::CATEGORY_SELECT . 'FROM category';

        $categories = DataManager::find($query, $params = null, 'Category');

        return $categories;
    }

}