<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 9/04/17
 * Time: 20:27
 */

namespace IBC\Kernel\EntityManager\DataManager;

use App\Entity\Town;

class TownDataManager extends DataManager
{

    const TOWN_SELECT = 'SELECT
                         town.id         AS Town$id_town,
                         town.name       AS Town$name,
                         town.population AS Town$population,
                         town.picture    AS Town$picture ';

    protected static function bindTownParams($params)
    {

        $stmt = $params[0];
        $town = $params[1];

        $stmt->bind_param('sss', $town->name, $town->picture, $town->population);

        return $stmt;

    }

    public static function persistTown(Town $town)
    {

        $query = "INSERT INTO town (name, picture, population) VALUES (?, ?, ?)";

        $townId = DataManager::persist($query, $town);

        return $townId;
    }

    public static function findTownByName($name)
    {

        $query = self::TOWN_SELECT . 'FROM   town
                  WHERE  town.name = ?';

        $town = DataManager::find($query, $params = array('s', $name), 'Town');

        return $town;

    }

    public static function findFriendsByTown(Town $town)
    {

        $query = TownDataManager::TOWN_SELECT. 'FROM friendship
                  LEFT JOIN town ON friendship.friendTown = town.id
                  WHERE friendship.town = ?';

        $townId = $town->getId();

        $friends = DataManager::find($query, $params = array('i', $townId), 'Town');

        return $friends;
    }

    public static function getMultipleTownsByIds($ids)
    {
        $query = self::TOWN_SELECT . 'FROM   town
                  WHERE town.id IN (';

        $arrayKeys = array_keys($ids);
        $lastArrayKey = array_pop($arrayKeys);
        $types = '';

        foreach($ids as $key => $id) {
            $types .= 'i';
            $query .= '?' ;

            //if is not the last element on the array
            if($key != $lastArrayKey) {
                $query .= ', ';
            }else{
                $query .= ')';
            }

            $params[0] = $types;
            $params[] = $id;
        }


        $towns = DataManager::find($query, $params, 'Town');

        return $towns;

    }

}