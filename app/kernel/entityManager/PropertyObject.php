<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 9/04/17
 * Time: 10:23
 */

namespace IBC\Kernel\EntityManager;

use App\Entity\Ad;
use App\Entity\Category;
use App\Entity\Content;
use App\Entity\Friendship;
use App\Entity\Organization;
use App\Entity\Role;
use App\Entity\Store;
use App\Entity\Town;
use App\Entity\User;


abstract class PropertyObject
{
    protected function buildObject($result, $objectType)
    {
        $objects = array();

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $object = call_user_func( 'self::build'.$objectType, $row);
            $objects[] = $object;
        }

        return $objects;

    }

    protected function buildFriendship($row)
    {

        $town = new Town($row['Town$id'], $row['Town$name'], $row['Town$picture'], $row['Town$population']);
        $friendTown = new Town($row['FriendTown$id'], $row['FriendTown$name'], $row['FriendTown$picture'], $row['FriendTown$population']);

        $friendship = new Friendship($row['Friendship$id'], $town, $friendTown, $row['Friendship$status'], $row['Friendship$date']);

        return $friendship;

    }

    protected function buildTown($row)
    {
        $town = new Town($row['Town$id_town'], $row['Town$name'], $row['Town$picture'], $row['Town$population']);

        return $town;

    }

    protected function buildUser($row)
    {

        $town = new Town($row['Town$id_town'], $row['Town$name'], $row['Town$picture'], $row['Town$population']);
        $role = new Role($row['Role$id'], $row['Role$name']);
        $user = new User($row['User$id_user'], $row['User$dni'], $row['User$name'], $row['User$firstSurname'],
                $row['User$secondSurname'], $row['User$birthdate'], $row['User$gender'], $row['User$email'],
                $role, $town, $row['User$picture']);

        return $user;
    }

    protected function buildOrganization($row)
    {

        $town = new Town($row['Town$id'], $row['Town$name'], $row['Town$picture'], $row['Town$population']);
        $organization = new Organization($row['Organization$id'], $row['Organization$name'],
                $row['Organization$description'], $town, $row['Organization$logo']);

        return $organization;

    }

    protected function buildStore($row)
    {

        $user = new User($row['User$id_user'], $row['User$dni'], $row['User$name'], $row['User$firstSurname'],
                $row['User$secondSurname'], $row['User$birthdate'], $row['User$gender'], $row['User$email'],
                $row['User$role'], $row['User$town'], $row['User$picture']);

        $store = new Store($row['Store$id_store'], $row['Store$name'], $user,  $row['Store$description'], $row['Store$logo']);

        return $store;
    }

    protected function buildAd($row)
    {

        $store = new Store($row['Store$id_store'], $row['Store$name'], $row['Store$user'], $row['Store$description'], $row['Store$logo']);

        $ad = new Ad($row['Ad$id_ad'], $store, $row['Ad$title'],  $row['Ad$description'], $row['Ad$photo']);

        return $ad;

    }

    protected function buildCategory($row){

        $category = new Category($row['Category$id'], $row['Category$name'], $row['Category$description']);

        return $category;

    }

    protected function buildContent($row)
    {

        $category = new Category($row['Category$id_category'], $row['Category$name'], $row['Category$description']);
        $author = new User($row['Author$id_user'], $row['Author$dni'], $row['Author$name'], $row['Author$firstSurname'],
                $row['Author$secondSurname'], $row['Author$birthdate'], $row['Author$gender'], $row['Author$email'],
                $row['Author$role'], $row['Author$town'], $row['Author$picture'] );
        $organization = new Organization($row['Organization$id'], $row['Organization$name'],
            $row['Organization$description'], $row['Organization$town'], $row['Organization$logo']);

        $content = new Content($row['Content$id_content'], $row['Content$title'], $row['Content$description'],
                $category, $row['Content$createdAt'], $author, $organization);

        return $content;
    }


    protected function buildArray($row)
    {
        unset($row['password']);

        return $row;
    }


}