<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 11/04/17
 * Time: 18:46
 */


function getMapping()
{
    $mapping = array(
        //All namespaces need to be defined here
        // Namespace => Directory
        'IBC\Kernel\EntityManager' => 'app/kernel/entityManager',
        'IBC\Kernel\EntityManager\DataManager' => 'app/kernel/entityManager/dataManager',
        'App\Entity' => 'src/Entity'

    );

    return $mapping;
}


function autoload($class)
{

    $initialParts = explode('\\', $class);

    $ns = array();

    for($i = 0; $i <= count($initialParts) -2; $i++){
        $ns[] = $initialParts[$i];
    }

    $namespace = implode('\\',$ns);
    $class = end($initialParts);

    $mapping = getMapping();

    foreach ($mapping as $definedNamespace => $path){
        if($namespace == $definedNamespace){
            include BASEPATH . $path.'/'.$class.'.php';
        }
    }
}


spl_autoload_register('autoload');

