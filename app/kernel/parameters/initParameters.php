<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 6/06/17
 * Time: 20:30
 */
$parameters_json = file_get_contents(__DIR__.'/parameters.json');
$parameters = json_decode($parameters_json, true);
foreach ($parameters as $parameter => $value){
    define($parameter, $value);
}