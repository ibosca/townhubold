<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 24/05/17
 * Time: 10:15
 */

namespace App\Entity;


class Role
{

    protected $id_role;
    public $name;

    const ROLE_SUPER_ADMIN = 1;
    const ROLE_ADMIN = 2;
    const ROLE_USER = 3;

    /**
     * Role constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->id_role = $id;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id_role;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public static function getRole($role)
    {
        switch ($role){
            case 1:
                return new Role(1, 'ROLE_SUPER_ADMIN');
                break;
            case 2:
                return new Role(2, 'ROLE_ADMIN');
                break;
            case 3:
                return new Role(3, 'ROLE_USER');
                break;
        }
    }




}