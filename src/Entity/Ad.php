<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 12/05/17
 * Time: 23:34
 */

namespace App\Entity;

use IBC\Kernel\EntityManager\DataManager\AdDataManager;
use IBC\Kernel\EntityManager\DataManager\UserDataManager;

class Ad
{

    protected $id_ad;
    public $store;
    public $title;
    public $description;
    public $photo;

    public function __construct($id = null, $store = null, $title = null, $description = null, $photo = null)
    {
        $this->id_ad = $id;
        $this->store = $store;
        $this->title = $title;
        $this->description = $description;
        $this->photo = $photo;

    }


    /* GETTERS AND SETTERS */

    public function getId()
    {
        return $this->id_ad;
    }



    /* DATABASE METHODS*/
    public function persist()
    {
        $adId = AdDataManager::persistAd($this);

        $this->id_ad = $adId;
    }

    public function viewedByUser(User $user)
    {
        $viewed = new Viewed();

        $viewed->setUser($user);
        $viewed->setAd($this);

        $viewed->persist();
    }

    public function getWatchers()
    {

        $watchers = UserDataManager::findWatchersOfAd($this);

        return $watchers;

    }


}