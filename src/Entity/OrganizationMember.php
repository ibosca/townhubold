<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 7/05/17
 * Time: 17:28
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\OrganizationMemberDataManager;

class OrganizationMember
{

    protected $id_organizationMember;
    public $user;
    public $organization;


    public function __construct($id_organizationMember = null, $user = null, $organization = null)
    {
        $this->id_organizationMember = $id_organizationMember;
        $this->user = $user;
        $this->organization = $organization;
    }

    /* GETTERS AND SETTERS */

    public function getUser()
    {
        return $this->user;
    }


    public function setUser($user)
    {
        $this->user = $user;
    }


    public function getOrganization()
    {
        return $this->organization;
    }


    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /* DATABASE METHODS */

    public function persist()
    {
        $memberId = OrganizationMemberDataManager::persistOrganizationMember($this);
        $this->id_organizationMember = $memberId;
    }




}