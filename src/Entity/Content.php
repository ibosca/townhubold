<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 20/05/17
 * Time: 18:52
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\ContentDataManager;
use IBC\Kernel\EntityManager\DataManager\UserDataManager;

class Content
{

    protected $id_content;
    public $title;
    public $description;
    public $category;
    public $createdAt;
    public $author;
    public $organization;

    public function __construct($id = null, $title = null, $description = null, $category = null, $createdAt = null, $author = null, $organization = null)
    {
        $this->id_content = $id;
        $this->title = $title;
        $this->description = $description;
        $this->category = $category;
        $this->createdAt = $createdAt;
        $this->author = $author;
        $this->organization = $organization;

    }

    /* GETTERS AND SETTERS */

    public function getId()
    {
        return $this->id_content;
    }


    public function getTitle()
    {
        return $this->title;
    }


    public function setTitle($title)
    {
        $this->title = $title;
    }


    public function getDescription()
    {
        return $this->description;
    }


    public function setDescription($description)
    {
        $this->description = $description;
    }


    public function getCategory()
    {
        return $this->category;
    }


    public function setCategory($category)
    {
        $this->category = $category;
    }


    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    public function getAuthor()
    {
        return $this->author;
    }


    public function setAuthor($author)
    {
        $this->author = $author;
    }


    /* DATABASE METHODS */
    public function persist()
    {
        $contentId = ContentDataManager::persistContent($this);

        $this->id_content = $contentId;
    }

    public function readByUser(User $user)
    {
        $read = new Read();

        $read->setUser($user);
        $read->setContent($this);

        $read->persist();
    }

    public function getReaders()
    {

        $readers = UserDataManager::findReadersOfContent($this);

        return $readers;
    }

    public function addReceiver(Town $town)
    {
        $receiver = new Receiver();

        $receiver->setContent($this);
        $receiver->setTown($town);

        $receiver->persist();
    }



}