<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 9/04/17
 * Time: 1:13
 */

namespace App\Entity;

use IBC\Kernel\EntityManager\DataManager\OrganizationDataManager;
use IBC\Kernel\EntityManager\PropertyObject;
use IBC\Kernel\EntityManager\DataManager\TownDataManager;

class Town extends PropertyObject
{
    protected $id_town;
    public $name;
    public $picture;
    public $population;

    public $friends;

    function __construct($id = null, $name = null, $picture = null, $population = null, $friends = null)
    {
        $this->id_town = $id;
        $this->name = $name;
        $this->picture = $picture;
        $this->population = $population;

        $this->friends = array();
    }


    /* GETTERS AND SETTERS */

    public function getId()
    {

        if($this->id_town == null){
            $town = TownDataManager::findTownByName($this->name)[0];
            $this->id_town = $town->id_town;
            $id = $this->id_town;
        }else{
            $id = $this->id_town;
        }

        return $id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function getPicture()
    {
        return $this->picture;
    }


    public function getPopulation()
    {
        return $this->population;
    }

    public function getFriends(){
        $friends = TownDataManager::findFriendsByTown($this);
        return $friends;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


    public function setPicture($picture)
    {
        $this->picture = $picture;
    }


    public function setPopulation($population)
    {
        $this->population = $population;
    }





    /* DATABASE METHODS */

    public function persist()
    {
        $townId = TownDataManager::persistTown($this);
        $this->id_town = $townId;
    }

    public function addFriend(Town $friendTown)
    {

        $friendship = new Friendship();

        $friendship->setTown($this);
        $friendship->setFriendTown($friendTown);

        $friendship->persist();


    }

    public function getOrganizations()
    {
        $organizations = OrganizationDataManager::findOrganizationsByTown($this);
        return $organizations;
    }




}