<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 24/05/17
 * Time: 11:41
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\CategoryDataManager;

class Category
{
    protected $id_category;
    public $name;
    public $description;

    /**
     * Category constructor.
     * @param $id_category
     * @param $name
     * @param $description
     */
    public function __construct($id_category, $name, $description)
    {
        $this->id_category = $id_category;
        $this->name = $name;
        $this->description = $description;
    }



    /*GETTERS AND SETTERS */


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id_category;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /* DATABASE METHODS */

    public static function getCategory($id)
    {
        $category = CategoryDataManager::findCategoryById($id)[0];

        return $category;
    }


}