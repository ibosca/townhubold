<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 25/05/17
 * Time: 11:20
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\ReceiverDataManager;

class Receiver
{

    protected $id_receiver;
    public $content;
    public $town;


    public function __construct($id_receiver = null, $content = null, $town = null)
    {
        $this->id_receiver = $id_receiver;
        $this->content = $content;
        $this->town = $town;
    }


    public function getId()
    {
        return $this->id_receiver;
    }


    public function getContent()
    {
        return $this->content;
    }


    public function setContent($content)
    {
        $this->content = $content;
    }


    public function getTown()
    {
        return $this->town;
    }


    public function setTown($town)
    {
        $this->town = $town;
    }

    public function persist(){
        $receiverId = ReceiverDataManager::persistReceiver($this);
        $this->id_receiver = $receiverId;
    }




}