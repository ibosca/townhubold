<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 7/05/17
 * Time: 0:12
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\OrganizationDataManager;
use IBC\Kernel\EntityManager\DataManager\OrganizationMemberDataManager;


class Organization
{

    protected $id_organization;
    public $name;
    public $description;
    public $town;
    public $logo;

    public $members;

    public function __construct($id = null, $name = null, $description = null, $town = null, $logo = null)
    {
        $this->id_organization = $id;
        $this->name = $name;
        $this->description = $description;
        $this->town = $town;
        $this->logo = $logo;

    }

    /* GETTERS AND SETTERS */

    public function getId()
    {
        if($this->id_organization == null){
            $organization = OrganizationDataManager::findOrganizationByTownAndName($this->town, $this->name)[0];
            $id = $organization->id_organization;
        }else{
            $id = $this->id_organization;
        }

        return $id;
    }




    /* DATABASE METHODS */

    public function persist()
    {
        $organizationId = OrganizationDataManager::persistOrganization($this);
        $this->id_organization = $organizationId;
    }

    public function addMember(User $user)
    {

        $member = new OrganizationMember();

        $member->setOrganization($this);
        $member->setUser($user);

        $member->persist();
    }

    public function getMembers()
    {

        $members = OrganizationMemberDataManager::findMembersByOrganization($this);

        return $members;
    }


}