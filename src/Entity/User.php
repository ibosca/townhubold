<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 5/05/17
 * Time: 19:33
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\AdDataManager;
use IBC\Kernel\EntityManager\DataManager\ContentDataManager;
use IBC\Kernel\EntityManager\DataManager\OrganizationMemberDataManager;
use IBC\Kernel\EntityManager\DataManager\UserDataManager;

class User
{

    protected $id_user;
    public $dni;
    public $password;
    public $name;
    public $firstSurname;
    public $secondSurname;
    public $birthdate;
    public $gender;
    public $email;
    public $role;
    public $town;
    public $picture;


    public function __construct($id_user = null, $dni = null, $name = null, $firstSurname = null,
                                $secondSurname = null, $birthdate = null, $gender = null, $email = null,
                                $role = null, $town = null, $picture = null)
    {
        $this->id_user = $id_user;
        $this->dni = $dni;
        $this->name = $name;
        $this->firstSurname = $firstSurname;
        $this->secondSurname = $secondSurname;
        $this->birthdate = $birthdate;
        $this->gender = $gender;
        $this->email = $email;
        $this->role = $role;
        $this->town = $town;
        $this->picture = $picture;
    }



    /* GETTERS AND SETTERS */



    public function getId()
    {
        if($this->id_user == null){
            $user = UserDataManager::findUserByEmail($this->email)[0];
            $id = $user->id_user;
        }else{
            $id = $this->id_user;
        }

        return $id;
    }

    /* DATABASE METHODS */

    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function verifyPassword($password)
    {

        if(password_verify($password, $this->password)){
            return true;
        }else{
            return false;
        }
    }

    public function persist()
    {
        $userId = UserDataManager::persistUser($this);
        $this->id_user = $userId;
    }

    public function getOrganizations()
    {

        $organizations = OrganizationMemberDataManager::findOrganizationsByUser($this);

        return $organizations;
    }

    public function getViewedAds()
    {

        $ads = AdDataManager::findViewedAdsByUser($this);

        return $ads;
    }

    public function getReadContents()
    {
        $contents = ContentDataManager::findReadContentsByUser($this);

        return $contents;
    }


}