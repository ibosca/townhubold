<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 25/05/17
 * Time: 9:05
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\ReadDataManager;

class Read
{

    protected $id_read;
    public $user;
    public $content;

    public function __construct($id = null, $user = null, $content = null)
    {
        $this->id_read = $id;
        $this->user = $user;
        $this->content = $content;
    }

    /* GETTERS AND SETTERS */

    /**
     * @param null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param null $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /* DATABASE METHODS */
    public function persist()
    {
        $readId = ReadDataManager::persistRead($this);
        $this->id_read = $readId;
    }




}