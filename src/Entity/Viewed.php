<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 20/05/17
 * Time: 11:18
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\ViewedDataManager;

class Viewed
{

    protected $id_viewed;
    public $user;
    public $ad;

    public function __construct($id = null, $user = null, $ad = null)
    {
        $this->id_viewed = $id;
        $this->user = $user;
        $this->ad = $ad;
    }

    /* GETTERS AND SETTERS */

    public function getUser()
    {
        return $this->user;
    }


    public function setUser($user)
    {
        $this->user = $user;
    }


    public function getAd()
    {
        return $this->ad;
    }


    public function setAd($ad)
    {
        $this->ad = $ad;
    }

    /* DATABASE METHODS */

    public function persist()
    {
        $viewedId = ViewedDataManager::persistViewed($this);
        $this->id_viewed = $viewedId;
    }



}