<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 26/04/17
 * Time: 17:24
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\FriendshipDataManager;

class Friendship
{

    protected $id_friendship;
    public $town;
    public $friendTown;
    public $status;
    public $friendshipStartDate;

    function __construct($id = null, $town = null, $friendTown = null, $status = 0, $friendshipStartDate = null)
    {

        $this->id_friendship = $id;
        $this->town = $town;
        $this->friendTown = $friendTown;
        $this->status = $status;
        $this->friendshipStartDate = $friendshipStartDate;
    }


    public function setTown($town)
    {
        $this->town = $town;
    }


    public function setFriendTown($friendTown)
    {
        $this->friendTown = $friendTown;
    }


    public function setStatus($status)
    {
        $this->status = $status;
    }


    public function setFriendshipStartDate($friendshipStartDate)
    {
        $this->friendshipStartDate = $friendshipStartDate;
    }




    public function persist()
    {
        $friendshipId = FriendshipDataManager::persistFriendship($this);
        $this->id_friendship = $friendshipId;
    }


}