<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 11/05/17
 * Time: 22:14
 */

namespace App\Entity;


use IBC\Kernel\EntityManager\DataManager\StoreDataManager;
use IBC\Kernel\EntityManager\PropertyObject;

class Store extends PropertyObject
{
    protected $id_store;
    public $name;
    public $user;
    public $description;
    public $logo;

    function __construct($id = null, $name = null, $user = null, $description = null, $logo = null)
    {
        $this->id_store = $id;
        $this->name = $name;
        $this->user = $user;
        $this->description = $description;
        $this->logo = $logo;

    }

    public function getId()
    {

        if($this->id_store == null){
            $store = StoreDataManager::findStoreByNameAndUser($this->name, $this->user)[0];
            $id = $store->id_store;
        }else{
            $id = $this->id_store;
        }

        return $id;
    }

    public function persist()
    {
        $storeId = StoreDataManager::persistStore($this);
        $this->id_store = $storeId;
    }


}